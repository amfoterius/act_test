from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from act_test import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'act_test.settings')
app = Celery('act_test', broker=settings.CELERY_BROKER)
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

@app.task(bind=True)
def dropbox_storage__create_after(self, name):
    from cloud_storage.storages.dropbox_storage import MyDropboxStorage
    from cloud_storage.storages.google_drive_storage import MyGoogleDriveStorage

    dropbox_storage = MyDropboxStorage()
    google_cloud_storage = MyGoogleDriveStorage()
    google_cloud_storage.save(name, dropbox_storage.open(name))


@app.task(bind=True)
def dropbox_storage__delete_after(self, name):
    from cloud_storage.storages.google_drive_storage import MyGoogleDriveStorage

    google_cloud_storage = MyGoogleDriveStorage()
    google_cloud_storage.delete(name)
