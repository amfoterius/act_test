1. Build containers, create database in **db** container
2. In web container: `cp ./act_test/settings.py.example ./act_test/settings.py && python manage.py migrate && python manage.py createsuperuser`
3. Set credentials by Dropbox storage (token) and Google Drive storage (json file)
