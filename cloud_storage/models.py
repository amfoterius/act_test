from django.db import models

from cloud_storage.storages.dropbox_storage import MyDropboxStorage

masterStorage = MyDropboxStorage()

class File(models.Model):
    path = models.CharField(max_length=500)
    blob_path = models.FileField(storage=masterStorage)

    @property
    def file_path(self):
        return '/%s/%s' % (self.path, self.blob_path.name)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.blob_path.name = '/%s/%s' % (self.path, self.blob_path.name)
        super().save(force_insert, force_update, using, update_fields)

    def __str__(self):
        return self.blob_path.name.split('/')[-1]

