from django.core.files.uploadedfile import InMemoryUploadedFile
from storages.backends.dropbox import DropBoxStorage

from act_test.celery import dropbox_storage__create_after, dropbox_storage__delete_after


class MyDropboxStorage(DropBoxStorage):

    def delete(self, name):
        result = super().delete(name)
        dropbox_storage__delete_after(name)
        return result


    def _save(self, name, content):
        self.client.files_upload(content.read(), name)
        return name

    def save(self, name: str, content: InMemoryUploadedFile, max_length=None) -> str:
        name = name.replace('\\', '/')
        result = self._save(name, content)
        dropbox_storage__create_after.apply_async((name,))

        return result