
class BaseStorageType:
    TYPE_ID = 1

    @staticmethod
    def get_type(self):
        return self.TYPE_ID

    def __str__(self):
        return 'Undefined'


class MasterStorageType:
    TYPE_ID = 2

    def __str__(self):
        return 'Master'


class SlaveStorageType(BaseStorageType):
    TYPE_ID = 2

    def __str__(self):
        return 'Slave'