from django.contrib import admin
from django.forms import ModelForm
from django.utils.encoding import force_str

from cloud_storage.models import File

class ReadOnlyPkForm(ModelForm):
    class Meta:
        model = File
        exclude = ('path','blob_path')

class FileAdmin(admin.ModelAdmin):
    change_list_template = 'admin/file/list.html'

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            kwargs['form'] = ReadOnlyPkForm
        return super().get_form(request, obj, **kwargs)



admin.site.register(File, FileAdmin)