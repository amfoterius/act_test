import collections

from django import template

register = template.Library()

def _uniq_append_in_list(in_arr, result_arr):
    """ Собирает массив из уникальных значений  in_arr для result_arr"""
    result = []

    result_arr = [str(file) for file in result_arr]
    for file in in_arr:
        if not str(file) in result_arr:
            result.append(file)

    return result

@register.filter
def group_by_dir(data):
    result = {}

    for row in data:
        if not row.grouper in result:
            result[row.grouper] = []

        result[row.grouper] += _uniq_append_in_list(row.list, result[row.grouper])

    od = collections.OrderedDict(sorted(result.items()))

    return od

@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)

